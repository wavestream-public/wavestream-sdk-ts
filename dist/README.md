<a href="https://www.wave.stream/">
    <img src="./wavestream_logo.png" alt="wavestream logo" title="Wavestream" align="right" height="45" />
</a>

# Wavestream-sdk-ts

The Wavestream client libary is a Javascript SDK written in typescript that allows for seamless communication with Wavestream's API.
Built-in methods make it easy to manage videos and webhooks operations.

## Table of contents

- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Runtime requirements](#runtime-requirements)
- [Usage](#usage)
  - [Example](#example)

## Getting Started

### Installation

Run following command to specify to `npm` where to fetch the wavestream sdk packages:

```bash
$ echo "@wavestream:registry=https://gitlab.com/api/v4/projects/39243899/packages/npm/" >> .npmrc
```

Install the package using `npm`:

```
$ npm install --save @wavestream/sdk-ts
```

After that, you can load the package:

```js
const wavestreamClient = require("@wavestream/sdk-ts);
```

If you are using ES Modules, you can use:

```js
import wavestreamClient from "@wavestream/sdk-ts";
```

## Runtime requirements

Node.js v14 or newer

## Usage

### Example

```javascript
// Initialize the client with your apiKey
const wavestream = new wavestreamClient({ apiKey: YOUR_API_KEY, endpoint?: API_URL, accessToken: YOUR_ACCESS_TOKEN });

//Create a new empty video container
let video = await wavestream.video.create({
  title: "Title of my video container",
  description: "Description of my video",
});

// Upload video source in the container
// The video processing starts automatically after the upload completes
wavestream.video.upload({
  videoId: video.id,
  filePath: "/home/user/Downloads/myFileName.mp4",
});

// Wait for the video to be processed
// Webhooks can be configured to notify the user when the upload and transcoding processes are completed

// Get video info
video = await wavestream.video.get(video.id);
video.outputs.forEach((vo) => console.log(vo.uri)); // Show outputs uri
```
