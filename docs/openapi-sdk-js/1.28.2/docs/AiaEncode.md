# WavestreamOpenapiSdkJs.AiaEncode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoId** | **String** |  | 
**inputBucket** | **String** |  | 
**inputKey** | **String** |  | 


