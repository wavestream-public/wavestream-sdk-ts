# WavestreamOpenapiSdkJs.App

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the app | [optional] [readonly] 
**name** | **String** | The name of the app | [optional] 
**secretKey** | **String** | The secretKey to use in API queries | [optional] 
**userId** | **String** | Id of the user who owns this app | [optional] [readonly] 
**s3AccessKey** | **String** | The access key for s3 | [optional] 
**s3SecretKey** | **String** | The secret key for s3 | [optional] 
**s3Region** | **String** | The region name for s3 | [optional] 
**s3Endpoint** | **String** | The Endpoint for s3 | [optional] 
**createdAt** | **Date** | Date of creation | [optional] 
**updatedAt** | **Date** | Date of last update | [optional] 


