# WavestreamOpenapiSdkJs.AppApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOneApp**](AppApi.md#getOneApp) | **GET** /apps/{appId} | Get an app
[**indexApps**](AppApi.md#indexApps) | **GET** /apps | Index of applications
[**renewAppSecret**](AppApi.md#renewAppSecret) | **POST** /apps/{appId}/renew-secret | Renew the api key
[**updateApp**](AppApi.md#updateApp) | **PUT** /apps/{appId} | Update an app



## getOneApp

> App getOneApp(appId)

Get an app

Fetch a single app

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: accessTokenAuth
let accessTokenAuth = defaultClient.authentications['accessTokenAuth'];
accessTokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//accessTokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.AppApi();
let appId = "appId_example"; // String | the id of the app
apiInstance.getOneApp(appId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app | 

### Return type

[**App**](App.md)

### Authorization

[accessTokenAuth](../README.md#accessTokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## indexApps

> [App] indexApps()

Index of applications

Index of applications

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: accessTokenAuth
let accessTokenAuth = defaultClient.authentications['accessTokenAuth'];
accessTokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//accessTokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.AppApi();
apiInstance.indexApps().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[App]**](App.md)

### Authorization

[accessTokenAuth](../README.md#accessTokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## renewAppSecret

> App renewAppSecret(appId)

Renew the api key

Renew the api key

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: accessTokenAuth
let accessTokenAuth = defaultClient.authentications['accessTokenAuth'];
accessTokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//accessTokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.AppApi();
let appId = "appId_example"; // String | the id of the app
apiInstance.renewAppSecret(appId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app | 

### Return type

[**App**](App.md)

### Authorization

[accessTokenAuth](../README.md#accessTokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## updateApp

> App updateApp(appId, updateAppRequest)

Update an app

Update an app.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: accessTokenAuth
let accessTokenAuth = defaultClient.authentications['accessTokenAuth'];
accessTokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//accessTokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.AppApi();
let appId = "appId_example"; // String | the id of the app
let updateAppRequest = new WavestreamOpenapiSdkJs.UpdateAppRequest(); // UpdateAppRequest | app to update in the system
apiInstance.updateApp(appId, updateAppRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app | 
 **updateAppRequest** | [**UpdateAppRequest**](UpdateAppRequest.md)| app to update in the system | 

### Return type

[**App**](App.md)

### Authorization

[accessTokenAuth](../README.md#accessTokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

