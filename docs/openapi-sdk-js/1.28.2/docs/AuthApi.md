# WavestreamOpenapiSdkJs.AuthApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**login**](AuthApi.md#login) | **GET** /auth/login | Register or log the user in
[**loginCallback**](AuthApi.md#loginCallback) | **GET** /auth/login/callback | Complete the login workflow
[**logout**](AuthApi.md#logout) | **GET** /auth/logout | Log the user out
[**logoutCallback**](AuthApi.md#logoutCallback) | **GET** /auth/logout/callback | Complete the logout workflow



## login

> login()

Register or log the user in

Register or log the user in

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';

let apiInstance = new WavestreamOpenapiSdkJs.AuthApi();
apiInstance.login().then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## loginCallback

> loginCallback()

Complete the login workflow

Complete the login workflow

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';

let apiInstance = new WavestreamOpenapiSdkJs.AuthApi();
apiInstance.loginCallback().then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## logout

> logout()

Log the user out

Log the user out

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';

let apiInstance = new WavestreamOpenapiSdkJs.AuthApi();
apiInstance.logout().then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## logoutCallback

> logoutCallback()

Complete the logout workflow

Complete the logout workflow

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';

let apiInstance = new WavestreamOpenapiSdkJs.AuthApi();
apiInstance.logoutCallback().then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json

