# WavestreamOpenapiSdkJs.Clip1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Object** |  | [optional] 
**status** | **Object** |  | [optional] 
**params** | [**ClipParams**](ClipParams.md) |  | 
**results** | [**ClipResult**](ClipResult.md) |  | [optional] 


