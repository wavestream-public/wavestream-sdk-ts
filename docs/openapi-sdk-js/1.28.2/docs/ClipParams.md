# WavestreamOpenapiSdkJs.ClipParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputUri** | **String** |  | 
**start** | **Number** |  | 
**end** | **Number** |  | 
**overlays** | [**[OverlayParamsDeprecated]**](OverlayParamsDeprecated.md) |  | [optional] 


