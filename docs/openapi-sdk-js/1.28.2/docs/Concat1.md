# WavestreamOpenapiSdkJs.Concat1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Object** |  | [optional] 
**status** | **Object** |  | [optional] 
**params** | [**ConcatParams**](ConcatParams.md) |  | 
**results** | [**ConcatResult**](ConcatResult.md) |  | [optional] 


