# WavestreamOpenapiSdkJs.CreateWebhookRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**[Event]**](Event.md) | Events which trigger the webhook | 
**endpoint** | **String** | Endpoint to send the query | 


