# WavestreamOpenapiSdkJs.Crop1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Object** |  | [optional] 
**status** | **Object** |  | [optional] 
**params** | [**CropParams**](CropParams.md) |  | 
**results** | [**CropResult**](CropResult.md) |  | [optional] 


