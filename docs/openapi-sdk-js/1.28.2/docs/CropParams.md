# WavestreamOpenapiSdkJs.CropParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputUri** | **String** |  | 
**right** | **Number** |  | 
**left** | **Number** |  | 
**top** | **Number** |  | 
**bottom** | **Number** |  | 


