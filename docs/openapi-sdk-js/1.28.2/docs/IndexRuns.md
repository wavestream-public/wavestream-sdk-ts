# WavestreamOpenapiSdkJs.IndexRuns

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[Run]**](Run.md) | array of runs | [optional] 
**pagination** | [**Pagination**](Pagination.md) |  | [optional] 


