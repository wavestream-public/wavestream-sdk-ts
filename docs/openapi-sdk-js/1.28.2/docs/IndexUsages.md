# WavestreamOpenapiSdkJs.IndexUsages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[Usage]**](Usage.md) | array of usages | [optional] 
**pagination** | [**Pagination**](Pagination.md) |  | [optional] 


