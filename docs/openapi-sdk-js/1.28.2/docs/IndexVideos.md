# WavestreamOpenapiSdkJs.IndexVideos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[Video]**](Video.md) | array of videos | [optional] 
**pagination** | [**Pagination**](Pagination.md) |  | [optional] 


