# WavestreamOpenapiSdkJs.IndexWebhooks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[Webhook]**](Webhook.md) | array of webhooks | [optional] 
**pagination** | [**Pagination**](Pagination.md) |  | [optional] 


