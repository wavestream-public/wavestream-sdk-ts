# WavestreamOpenapiSdkJs.IndexWorkflowExecutions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[WorkflowExecution]**](WorkflowExecution.md) | array of workflow executions | [optional] 
**pagination** | [**Pagination**](Pagination.md) |  | [optional] 


