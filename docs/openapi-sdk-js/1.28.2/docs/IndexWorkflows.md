# WavestreamOpenapiSdkJs.IndexWorkflows

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[Workflow]**](Workflow.md) | array of workflows | [optional] 
**pagination** | [**Pagination**](Pagination.md) |  | [optional] 


