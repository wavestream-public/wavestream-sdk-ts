# WavestreamOpenapiSdkJs.InputValidationIssueAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_in** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**value** | **Object** |  | [optional] 



## Enum: InEnum


* `body` (value: `"body"`)

* `header` (value: `"header"`)

* `path` (value: `"path"`)

* `query` (value: `"query"`)




