# WavestreamOpenapiSdkJs.InputValidationProblemAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issues** | [**[InputValidationIssue]**](InputValidationIssue.md) |  | [optional] 


