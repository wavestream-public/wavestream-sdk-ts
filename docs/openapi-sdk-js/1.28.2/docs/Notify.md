# WavestreamOpenapiSdkJs.Notify

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Task UUID generated by the API. Read-only field. | [optional] [readonly] 
**label** | **String** | user defined label, accept only alphanumeric characters, without spaces | 
**type** | **Object** |  | 
**needs** | **[String]** | list of tasks that needs to run before this one as it depends on them. The values are tasks names defined earlier. | [optional] 
**status** | **Object** |  | [optional] 
**error** | **Object** |  | [optional] [readonly] 
**startedAt** | **Date** | start time | [optional] [readonly] 
**endedAt** | **Date** | end time | [optional] [readonly] 
**createdAt** | **Date** | created time | [optional] [readonly] 
**updatedAt** | **Date** | updated time | [optional] [readonly] 
**params** | [**NotifyParams**](NotifyParams.md) |  | 
**results** | [**NotifyResult**](NotifyResult.md) |  | [optional] 


