# WavestreamOpenapiSdkJs.Notify1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Object** |  | [optional] 
**status** | **Object** |  | [optional] 
**params** | [**NotifyParams**](NotifyParams.md) |  | 
**results** | [**NotifyResult**](NotifyResult.md) |  | [optional] 


