# WavestreamOpenapiSdkJs.NotifyParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endpoint** | **String** |  | 
**message** | **String** |  | 


