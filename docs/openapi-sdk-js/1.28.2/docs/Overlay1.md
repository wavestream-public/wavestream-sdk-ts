# WavestreamOpenapiSdkJs.Overlay1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Object** |  | [optional] 
**status** | **Object** |  | [optional] 
**params** | [**OverlayParams**](OverlayParams.md) |  | 
**results** | [**OverlayResult**](OverlayResult.md) |  | [optional] 


