# WavestreamOpenapiSdkJs.OverlayParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputUri** | **String** |  | 
**overlays** | [**[OverlayParamsDeprecated]**](OverlayParamsDeprecated.md) |  | [optional] 


