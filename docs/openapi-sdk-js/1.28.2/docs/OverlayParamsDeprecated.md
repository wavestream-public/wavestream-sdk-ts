# WavestreamOpenapiSdkJs.OverlayParamsDeprecated

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pictureUri** | **String** | if you want a transparent background, the image should be a png with transparent background | [optional] 
**pictureBase64** | **String** | base64 encoded image | [optional] 
**text** | **String** |  | [optional] 
**textSize** | **Number** | Size of the overlay text in pixels | [optional] 
**startTime** | **Number** |  | 
**endTime** | **Number** |  | 
**x** | **Number** |  | 
**y** | **Number** |  | 
**width** | **Number** |  | [optional] 
**height** | **Number** |  | [optional] 
**rotation** | **Number** |  | [optional] 


