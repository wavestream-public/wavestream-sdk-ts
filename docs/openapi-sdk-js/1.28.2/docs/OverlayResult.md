# WavestreamOpenapiSdkJs.OverlayResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outputUri** | **String** |  | [optional] 


