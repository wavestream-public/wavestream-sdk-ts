# WavestreamOpenapiSdkJs.Pagination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **Number** | page number | [optional] 
**pageSize** | **Number** | Number of elements in the page | [optional] 
**totalNbOfItems** | **Number** | Number of elements in database | [optional] 


