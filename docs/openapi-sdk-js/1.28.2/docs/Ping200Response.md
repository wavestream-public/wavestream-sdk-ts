# WavestreamOpenapiSdkJs.Ping200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | [optional] [default to &#39;OK&#39;]


