# WavestreamOpenapiSdkJs.PingApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ping**](PingApi.md#ping) | **GET** /ping | Pinging the service



## ping

> Ping200Response ping()

Pinging the service

Checking service reachability. If a 200 is sent back, the service is up and reachable, however, it does not mean that the service is healthy.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';

let apiInstance = new WavestreamOpenapiSdkJs.PingApi();
apiInstance.ping().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Ping200Response**](Ping200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

