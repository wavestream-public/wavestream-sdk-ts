# WavestreamOpenapiSdkJs.Publish1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Object** |  | [optional] 
**status** | **Object** |  | [optional] 
**params** | [**PublishParams**](PublishParams.md) |  | 
**results** | [**PublishResult**](PublishResult.md) |  | [optional] 


