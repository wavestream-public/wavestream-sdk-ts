# WavestreamOpenapiSdkJs.PublishParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputUri** | **String** |  | [optional] 
**outputDescription** | **String** |  | [optional] 
**outputFilename** | **String** |  | 
**outputTitle** | **String** |  | 


