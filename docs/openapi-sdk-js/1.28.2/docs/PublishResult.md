# WavestreamOpenapiSdkJs.PublishResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outputUri** | **String** |  | [optional] 
**videoId** | **String** |  | [optional] 


