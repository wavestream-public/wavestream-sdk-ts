# WavestreamOpenapiSdkJs.Run

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the run | 
**status** | **String** | The status of the run | [default to &#39;running&#39;]
**params** | **Object** | The run input params - format depends on the executed workflow | 
**result** | **Object** | The run result - format depends on the executed workflow | [optional] 
**error** | **String** | The error raised during the run | [optional] 
**workflowId** | **String** | The id of the workflow | 
**workflow** | [**Workflow**](Workflow.md) |  | 
**appId** | **String** | Id of the app which owns this run | [readonly] 
**startedAt** | **Date** | start time | [readonly] 
**endedAt** | **Date** | end time | [optional] [readonly] 
**createdAt** | **Date** | Date of creation | 
**updatedAt** | **Date** | Date of last update | 
**tasks** | [**[Task]**](Task.md) |  | [optional] 



## Enum: StatusEnum


* `running` (value: `"running"`)

* `completed` (value: `"completed"`)

* `failed` (value: `"failed"`)

* `canceled` (value: `"canceled"`)

* `terminated` (value: `"terminated"`)

* `continuedAsNew` (value: `"continuedAsNew"`)

* `timedOut` (value: `"timedOut"`)




