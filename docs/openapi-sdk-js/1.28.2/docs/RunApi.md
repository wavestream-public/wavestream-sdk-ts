# WavestreamOpenapiSdkJs.RunApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOneRun**](RunApi.md#getOneRun) | **GET** /runs/{runId} | Get a run
[**indexRuns**](RunApi.md#indexRuns) | **GET** /runs | Index runs



## getOneRun

> Run getOneRun(runId)

Get a run

Fetch a single run

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.RunApi();
let runId = "runId_example"; // String | the uuid of the run
apiInstance.getOneRun(runId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **runId** | **String**| the uuid of the run | 

### Return type

[**Run**](Run.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## indexRuns

> IndexRuns indexRuns(opts)

Index runs

Index runs

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.RunApi();
let opts = {
  'sort': "sort_example", // String | Use to sort results. You can specify by which value it will be sorted. example - \"sort=title:asc\" \"sort=createdAt:desc\"
  'page': 56, // Number | The number of the page you want to get. Default value = 1
  'pageSize': 56 // Number | The number of element a page can contains. Default value = 10
};
apiInstance.indexRuns(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort** | **String**| Use to sort results. You can specify by which value it will be sorted. example - \&quot;sort&#x3D;title:asc\&quot; \&quot;sort&#x3D;createdAt:desc\&quot; | [optional] 
 **page** | **Number**| The number of the page you want to get. Default value &#x3D; 1 | [optional] 
 **pageSize** | **Number**| The number of element a page can contains. Default value &#x3D; 10 | [optional] 

### Return type

[**IndexRuns**](IndexRuns.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

