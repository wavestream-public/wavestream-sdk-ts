# WavestreamOpenapiSdkJs.UpdateAppRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**s3AccessKey** | **String** | The S3 access key | [optional] 
**s3SecretKey** | **String** | The S3 secret key | [optional] 
**s3Region** | **String** | The S3 region | [optional] 
**s3Endpoint** | **String** | The S3 endpoint | [optional] 


