# WavestreamOpenapiSdkJs.UpdateVideoRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | The title of the video | [optional] 
**description** | **String** | The description of the video | [optional] 
**skipProcessing** | **Boolean** | Flag to disable (skip) the processing steps | [optional] [default to false]
**accessType** | **String** | define the accessType of the video, if private the video URI can only be accessed with a signed url | [optional] [default to &#39;public&#39;]



## Enum: AccessTypeEnum


* `public` (value: `"public"`)

* `private` (value: `"private"`)




