# WavestreamOpenapiSdkJs.UpdateWebhookRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**[Event]**](Event.md) | Events which trigger the webhook | [optional] 
**endpoint** | **String** | Endpoint to send the query | [optional] 


