# WavestreamOpenapiSdkJs.Usage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the video | 
**date** | **String** | Date | 
**processing** | [**UsageProcessing**](UsageProcessing.md) |  | 
**storage** | [**UsageStorage**](UsageStorage.md) |  | 
**cdn** | [**UsageCdn**](UsageCdn.md) |  | 
**appId** | **String** | Id of the app who owns this usage | 
**createdAt** | **Date** | Date of creation | 
**updatedAt** | **Date** | Date of last update | 


