# WavestreamOpenapiSdkJs.UsageApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**indexUsages**](UsageApi.md#indexUsages) | **GET** /usages | Index of usages



## indexUsages

> IndexUsages indexUsages(opts)

Index of usages

Index of usages

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.UsageApi();
let opts = {
  'date': "date_example", // String | The usage date. Use date format yyyy-mm-yy, and gt(), lt() as operators.
  'page': 56, // Number | The number of the page you want to get. Default value = 1
  'pageSize': 56, // Number | The number of element a page can contains. Default value = 10
  'sort': "sort_example" // String | Use to sort results. You can specify by which value it will be sorted. example - \"sort=title:asc\" \"sort=createdAt:desc\"
};
apiInstance.indexUsages(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **String**| The usage date. Use date format yyyy-mm-yy, and gt(), lt() as operators. | [optional] 
 **page** | **Number**| The number of the page you want to get. Default value &#x3D; 1 | [optional] 
 **pageSize** | **Number**| The number of element a page can contains. Default value &#x3D; 10 | [optional] 
 **sort** | **String**| Use to sort results. You can specify by which value it will be sorted. example - \&quot;sort&#x3D;title:asc\&quot; \&quot;sort&#x3D;createdAt:desc\&quot; | [optional] 

### Return type

[**IndexUsages**](IndexUsages.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

