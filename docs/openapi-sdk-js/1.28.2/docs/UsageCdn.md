# WavestreamOpenapiSdkJs.UsageCdn

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **Number** | Bandwith consumed in MB | [optional] 
**cost** | **String** | Cost of the usage | [optional] 


