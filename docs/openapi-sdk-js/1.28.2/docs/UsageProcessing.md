# WavestreamOpenapiSdkJs.UsageProcessing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **Number** | Duration of video processed in Second | [optional] 
**cost** | **String** | Type of the usage | [optional] 


