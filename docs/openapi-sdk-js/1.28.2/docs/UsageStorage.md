# WavestreamOpenapiSdkJs.UsageStorage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **Number** | Quantity of video stored in GB | [optional] 
**cost** | **String** | Type of the usage | [optional] 


