# WavestreamOpenapiSdkJs.User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the user | [optional] [readonly] 
**auth0Id** | **String** | The id of the user in Auth0 | [optional] 
**username** | **String** | The username of the user | [optional] 
**email** | **String** | The email of the user | [optional] 
**avatarUri** | **String** | The avatar of the user (uri) | [optional] 
**createdAt** | **Date** | Date of creation | [optional] 
**updatedAt** | **Date** | Date of last update | [optional] 


