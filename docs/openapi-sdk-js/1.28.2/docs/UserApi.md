# WavestreamOpenapiSdkJs.UserApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersMe**](UserApi.md#usersMe) | **GET** /users/me | Get the user&#39;s info



## usersMe

> User usersMe()

Get the user&#39;s info

Get the user&#39;s info

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: accessTokenAuth
let accessTokenAuth = defaultClient.authentications['accessTokenAuth'];
accessTokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//accessTokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.UserApi();
apiInstance.usersMe().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

[accessTokenAuth](../README.md#accessTokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

