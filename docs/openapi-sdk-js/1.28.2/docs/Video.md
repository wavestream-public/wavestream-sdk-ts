# WavestreamOpenapiSdkJs.Video

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the video | [optional] 
**title** | **String** | The title of the video | [optional] 
**description** | **String** | The description of the video | [optional] 
**status** | **String** | The status of the video | [optional] [default to &#39;empty&#39;]
**skipProcessing** | **Boolean** | Flag to disable (skip) the processing steps | [optional] [default to false]
**accessType** | **String** | define the accessType of the video, if private the video URI can only be accessed with a signed url | [optional] [default to &#39;public&#39;]
**transcodedAt** | **Date** | date of transcoding | [optional] 
**errorMessage** | **String** | A message describing the error if any | [optional] 
**destinationUri** | **String** | The uri of the main video output. Deprecated - Use videoOutputs instead | [optional] 
**input** | [**VideoInput**](VideoInput.md) |  | [optional] 
**outputs** | [**[VideoOutput]**](VideoOutput.md) | array of output files of the video | [optional] 
**appId** | **String** | Id of the app who owns this video | [optional] 
**createdAt** | **Date** | Date of creation | [optional] 
**updatedAt** | **Date** | Date of last update | [optional] 



## Enum: StatusEnum


* `empty` (value: `"empty"`)

* `in_progress` (value: `"in_progress"`)

* `ready` (value: `"ready"`)

* `error` (value: `"error"`)





## Enum: AccessTypeEnum


* `public` (value: `"public"`)

* `private` (value: `"private"`)




