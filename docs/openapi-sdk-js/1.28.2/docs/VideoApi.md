# WavestreamOpenapiSdkJs.VideoApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVideo**](VideoApi.md#createVideo) | **POST** /videos | Create a new video
[**deleteVideo**](VideoApi.md#deleteVideo) | **DELETE** /videos/{videoId} | Delete a video
[**getOneVideo**](VideoApi.md#getOneVideo) | **GET** /videos/{videoId} | Get a video
[**indexVideos**](VideoApi.md#indexVideos) | **GET** /videos | Index of videos
[**updateVideo**](VideoApi.md#updateVideo) | **PUT** /videos/{videoId} | Update a video



## createVideo

> Video createVideo(createVideoRequest)

Create a new video

Create a new video.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoApi();
let createVideoRequest = new WavestreamOpenapiSdkJs.CreateVideoRequest(); // CreateVideoRequest | video to create in the system
apiInstance.createVideo(createVideoRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createVideoRequest** | [**CreateVideoRequest**](CreateVideoRequest.md)| video to create in the system | 

### Return type

[**Video**](Video.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json


## deleteVideo

> deleteVideo(videoId)

Delete a video

Delete the video matching the given id

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoApi();
let videoId = "videoId_example"; // String | the uuid of the video
apiInstance.deleteVideo(videoId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## getOneVideo

> Video getOneVideo(videoId)

Get a video

Fetch a single video

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoApi();
let videoId = "videoId_example"; // String | the uuid of the video
apiInstance.getOneVideo(videoId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 

### Return type

[**Video**](Video.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## indexVideos

> IndexVideos indexVideos(opts)

Index of videos

Index of videos

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoApi();
let opts = {
  'title': "title_example", // String | The video title. By default, all the titles that contains the string provided will be returned. Use eq() if you want to get an exact match.
  'createdAt': "createdAt_example", // String | The date to compare with creation date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than.
  'updateddAt': "updateddAt_example", // String | The date to compare with the last update date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than.
  'sort': "sort_example", // String | Use to sort results. You can specify by which value it will be sorted. example - \"sort=title:asc\" \"sort=createdAt:desc\"
  'page': 56, // Number | The number of the page you want to get. Default value = 1
  'pageSize': 56 // Number | The number of element a page can contains. Default value = 10
};
apiInstance.indexVideos(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **title** | **String**| The video title. By default, all the titles that contains the string provided will be returned. Use eq() if you want to get an exact match. | [optional] 
 **createdAt** | **String**| The date to compare with creation date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than. | [optional] 
 **updateddAt** | **String**| The date to compare with the last update date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than. | [optional] 
 **sort** | **String**| Use to sort results. You can specify by which value it will be sorted. example - \&quot;sort&#x3D;title:asc\&quot; \&quot;sort&#x3D;createdAt:desc\&quot; | [optional] 
 **page** | **Number**| The number of the page you want to get. Default value &#x3D; 1 | [optional] 
 **pageSize** | **Number**| The number of element a page can contains. Default value &#x3D; 10 | [optional] 

### Return type

[**IndexVideos**](IndexVideos.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## updateVideo

> updateVideo(videoId, opts)

Update a video

Update a video.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoApi();
let videoId = "videoId_example"; // String | the uuid of the video
let opts = {
  'updateVideoRequest': new WavestreamOpenapiSdkJs.UpdateVideoRequest() // UpdateVideoRequest | video to update in the system
};
apiInstance.updateVideo(videoId, opts).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 
 **updateVideoRequest** | [**UpdateVideoRequest**](UpdateVideoRequest.md)| video to update in the system | [optional] 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/problem+json

