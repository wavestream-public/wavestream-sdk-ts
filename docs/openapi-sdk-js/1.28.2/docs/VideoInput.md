# WavestreamOpenapiSdkJs.VideoInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the video input | [optional] 
**label** | **String** | The label of the video input | [optional] 
**format** | **String** | The format of the video input | [optional] 
**videoCodec** | **String** | The video codec of the video input | [optional] 
**audioCodec** | **String** | The audio codec of the video input | [optional] 
**quality** | **String** | The quality of the video input | [optional] 
**uri** | **String** | The uri of the playlist of the video input | [optional] 
**duration** | **Number** | The duration the video input | [optional] 
**width** | **Number** | The width of the video input | [optional] 
**height** | **Number** | The height of the video input | [optional] 
**bitrate** | **String** | The bitrate of the video input | [optional] 
**aspectRatio** | **String** | The aspect ratio of the video input (4/3, 16/9) | [optional] 
**size** | **Number** | The size of the video input. | [optional] 


