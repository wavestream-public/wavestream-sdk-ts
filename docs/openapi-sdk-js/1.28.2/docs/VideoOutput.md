# WavestreamOpenapiSdkJs.VideoOutput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the video output | [optional] 
**label** | **String** | The label of the video output | [optional] 
**format** | **String** | The format of the video output | [optional] 
**videoCodec** | **String** | The video codec of the video output | [optional] 
**audioCodec** | **String** | The audio codec of the video output | [optional] 
**quality** | **String** | The quality of the video output | [optional] 
**masterPlaylistUri** | **String** | The uri of the master playlist of the video output. | [optional] 
**uri** | **String** | The uri of the playlist of the video output | [optional] 
**duration** | **Number** | The duration the video output | [optional] 
**width** | **Number** | The width of the video output | [optional] 
**height** | **Number** | The height of the video output | [optional] 
**bitrate** | **Number** | The bitrate of the video output | [optional] 
**aspectRatio** | **String** | The aspect ratio of the video output (4/3, 16/9) | [optional] 
**posterUri** | **String** | The uri of the poster of the video output | [optional] 
**size** | **Number** | The size of the video output. | [optional] 


