# WavestreamOpenapiSdkJs.VideoSourceUploadApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**headVideoSourceContainerId**](VideoSourceUploadApi.md#headVideoSourceContainerId) | **HEAD** /videos/{videoId}/source/{containerId} | Head container Id
[**optionsVideoSource**](VideoSourceUploadApi.md#optionsVideoSource) | **OPTIONS** /videos/{videoId}/source | Allow cross origin requests
[**optionsVideoSourceContainerId**](VideoSourceUploadApi.md#optionsVideoSourceContainerId) | **OPTIONS** /videos/{videoId}/source/{containerId} | Allow cross origin requests
[**postVideoSourceContainer**](VideoSourceUploadApi.md#postVideoSourceContainer) | **POST** /videos/{videoId}/source | Create the file container
[**uploadVideoSourceSegment**](VideoSourceUploadApi.md#uploadVideoSourceSegment) | **PATCH** /videos/{videoId}/source/{containerId} | Uploading file segment



## headVideoSourceContainerId

> headVideoSourceContainerId(videoId, containerId)

Head container Id

Endpoint to check if the file is uploaded partially or entirely. If Upload-Offest is equal to Content-Length, then file upload is completed

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoSourceUploadApi();
let videoId = "videoId_example"; // String | the uuid of the video
let containerId = "containerId_example"; // String | the uuid of the container
apiInstance.headVideoSourceContainerId(videoId, containerId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 
 **containerId** | **String**| the uuid of the container | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## optionsVideoSource

> optionsVideoSource(videoId)

Allow cross origin requests

Allow cross origin requests

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoSourceUploadApi();
let videoId = "videoId_example"; // String | the uuid of the video
apiInstance.optionsVideoSource(videoId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## optionsVideoSourceContainerId

> optionsVideoSourceContainerId(videoId, containerId)

Allow cross origin requests

Allow cross origin requests

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoSourceUploadApi();
let videoId = "videoId_example"; // String | the uuid of the video
let containerId = "containerId_example"; // String | the uuid of the container
apiInstance.optionsVideoSourceContainerId(videoId, containerId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 
 **containerId** | **String**| the uuid of the container | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## postVideoSourceContainer

> postVideoSourceContainer(videoId, opts)

Create the file container

Create the file container and redirect the client with temporary file id in Location header

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoSourceUploadApi();
let videoId = "videoId_example"; // String | the uuid of the video
let opts = {
  'tusResumable': "'1.0.0'" // String | current version of the Tus library
};
apiInstance.postVideoSourceContainer(videoId, opts).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 
 **tusResumable** | **String**| current version of the Tus library | [optional] [default to &#39;1.0.0&#39;]

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## uploadVideoSourceSegment

> uploadVideoSourceSegment(videoId, containerId, opts)

Uploading file segment

Uploading file segment by chunk, using the header Upload-offset to keep track of the progression

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.VideoSourceUploadApi();
let videoId = "videoId_example"; // String | the uuid of the video
let containerId = "containerId_example"; // String | the uuid of the container
let opts = {
  'tusResumable': "'1.0.0'", // String | current version of the Tus library
  'uploadOffset': 0, // Number | Size in bytes of the upload offset
  'contentLength': 3.4 // Number | Size in bytes of the segment
};
apiInstance.uploadVideoSourceSegment(videoId, containerId, opts).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **String**| the uuid of the video | 
 **containerId** | **String**| the uuid of the container | 
 **tusResumable** | **String**| current version of the Tus library | [optional] [default to &#39;1.0.0&#39;]
 **uploadOffset** | **Number**| Size in bytes of the upload offset | [optional] [default to 0]
 **contentLength** | **Number**| Size in bytes of the segment | [optional] 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json

