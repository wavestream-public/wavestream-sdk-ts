# WavestreamOpenapiSdkJs.Webhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the webhook | [optional] 
**events** | [**[Event]**](Event.md) | Events which trigger the webhook | [optional] 
**endpoint** | **String** | Endpoint to send the query | [optional] 
**appId** | **String** | Id of the app who owns this webhook | [optional] 
**createdAt** | **Date** | Date of creation | [optional] 
**updatedAt** | **Date** | Date of last update | [optional] 


