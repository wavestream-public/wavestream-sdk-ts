# WavestreamOpenapiSdkJs.WebhookApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createWebhook**](WebhookApi.md#createWebhook) | **POST** /webhooks | Create a new webhook
[**deleteWebhook**](WebhookApi.md#deleteWebhook) | **DELETE** /webhooks/{webhookId} | Delete a webhook
[**getOneWebhook**](WebhookApi.md#getOneWebhook) | **GET** /webhooks/{webhookId} | Get a webhook
[**indexWebhooks**](WebhookApi.md#indexWebhooks) | **GET** /webhooks | Index of webhooks
[**updateWebhook**](WebhookApi.md#updateWebhook) | **PUT** /webhooks/{webhookId} | Update a webhook



## createWebhook

> Webhook createWebhook(createWebhookRequest)

Create a new webhook

Create a new webhook.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WebhookApi();
let createWebhookRequest = new WavestreamOpenapiSdkJs.CreateWebhookRequest(); // CreateWebhookRequest | webhook to create in the system
apiInstance.createWebhook(createWebhookRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createWebhookRequest** | [**CreateWebhookRequest**](CreateWebhookRequest.md)| webhook to create in the system | 

### Return type

[**Webhook**](Webhook.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json


## deleteWebhook

> deleteWebhook(webhookId)

Delete a webhook

Delete the webhook matching the given id

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WebhookApi();
let webhookId = "webhookId_example"; // String | the uuid of the webhook
apiInstance.deleteWebhook(webhookId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **String**| the uuid of the webhook | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## getOneWebhook

> Webhook getOneWebhook(webhookId)

Get a webhook

Fetch a single webhook

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WebhookApi();
let webhookId = "webhookId_example"; // String | the uuid of the webhook
apiInstance.getOneWebhook(webhookId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **String**| the uuid of the webhook | 

### Return type

[**Webhook**](Webhook.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## indexWebhooks

> IndexWebhooks indexWebhooks(opts)

Index of webhooks

Index of webhooks

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WebhookApi();
let opts = {
  'endpoint': "endpoint_example", // String | A text contained in the webhook endpoint.
  'createdAt': "createdAt_example", // String | The date to compare with creation date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than.
  'updateddAt': "updateddAt_example", // String | The date to compare with the last update date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than.
  'sort': "sort_example", // String | Use to sort results. You can specify by which value it will be sorted. example - \"sort=title:asc\" \"sort=createdAt:desc\"
  'pageSize': 56, // Number | The number of element a page can contains. Default value = 10
  'page': 56 // Number | The number of the page you want to get. Default value = 1
};
apiInstance.indexWebhooks(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endpoint** | **String**| A text contained in the webhook endpoint. | [optional] 
 **createdAt** | **String**| The date to compare with creation date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than. | [optional] 
 **updateddAt** | **String**| The date to compare with the last update date. Use date format yyyy-mm-yy, use gt(), lt() before the date for greater than or less than. | [optional] 
 **sort** | **String**| Use to sort results. You can specify by which value it will be sorted. example - \&quot;sort&#x3D;title:asc\&quot; \&quot;sort&#x3D;createdAt:desc\&quot; | [optional] 
 **pageSize** | **Number**| The number of element a page can contains. Default value &#x3D; 10 | [optional] 
 **page** | **Number**| The number of the page you want to get. Default value &#x3D; 1 | [optional] 

### Return type

[**IndexWebhooks**](IndexWebhooks.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## updateWebhook

> updateWebhook(webhookId, updateWebhookRequest)

Update a webhook

Update a webhook.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WebhookApi();
let webhookId = "webhookId_example"; // String | the uuid of the webhook
let updateWebhookRequest = new WavestreamOpenapiSdkJs.UpdateWebhookRequest(); // UpdateWebhookRequest | webhook to update in the system
apiInstance.updateWebhook(webhookId, updateWebhookRequest).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **String**| the uuid of the webhook | 
 **updateWebhookRequest** | [**UpdateWebhookRequest**](UpdateWebhookRequest.md)| webhook to update in the system | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/problem+json

