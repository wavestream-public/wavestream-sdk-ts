# WavestreamOpenapiSdkJs.Workflow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the workflow | 
**name** | **String** | The name of the workflow | 
**description** | **String** | The description of the workflow | [optional] 
**appId** | **String** | Id of the app who owns this workflow | 
**createdAt** | **Date** | Date of creation | 
**updatedAt** | **Date** | Date of last update | 


