# WavestreamOpenapiSdkJs.WorkflowApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createWorkflow**](WorkflowApi.md#createWorkflow) | **POST** /workflows | Create a new workflow
[**deleteWorkflow**](WorkflowApi.md#deleteWorkflow) | **DELETE** /workflows/{workflowId} | Delete a workflow
[**executeWorkflow**](WorkflowApi.md#executeWorkflow) | **POST** /workflows/{workflowId}/execute | Execute a workflow
[**getOneWorkflow**](WorkflowApi.md#getOneWorkflow) | **GET** /workflows/{workflowId} | Get a workflow
[**indexWorkflows**](WorkflowApi.md#indexWorkflows) | **GET** /workflows | Index workflows
[**updateWorkflow**](WorkflowApi.md#updateWorkflow) | **PUT** /workflows/{workflowId} | Update a workflow



## createWorkflow

> Workflow createWorkflow(workflowCreateInputs)

Create a new workflow

Create a new workflow.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowApi();
let workflowCreateInputs = new WavestreamOpenapiSdkJs.WorkflowCreateInputs(); // WorkflowCreateInputs | workflow to create
apiInstance.createWorkflow(workflowCreateInputs).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowCreateInputs** | [**WorkflowCreateInputs**](WorkflowCreateInputs.md)| workflow to create | 

### Return type

[**Workflow**](Workflow.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json


## deleteWorkflow

> deleteWorkflow(workflowId)

Delete a workflow

Delete the workflow.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowApi();
let workflowId = "workflowId_example"; // String | the uuid of the workflow
apiInstance.deleteWorkflow(workflowId).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **String**| the uuid of the workflow | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/problem+json


## executeWorkflow

> Run executeWorkflow(workflowId, opts)

Execute a workflow

(Deprecated) Execute a workflow. Use a POST request to /workflow-executions instead

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowApi();
let workflowId = "workflowId_example"; // String | the uuid of the workflow
let opts = {
  'workflowExecuteInputs': new WavestreamOpenapiSdkJs.WorkflowExecuteInputs() // WorkflowExecuteInputs | Params to be passed to the workflow which will be executed.
};
apiInstance.executeWorkflow(workflowId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **String**| the uuid of the workflow | 
 **workflowExecuteInputs** | [**WorkflowExecuteInputs**](WorkflowExecuteInputs.md)| Params to be passed to the workflow which will be executed. | [optional] 

### Return type

[**Run**](Run.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json


## getOneWorkflow

> Workflow getOneWorkflow(workflowId)

Get a workflow

Fetch a workflow

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowApi();
let workflowId = "workflowId_example"; // String | the uuid of the workflow
apiInstance.getOneWorkflow(workflowId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **String**| the uuid of the workflow | 

### Return type

[**Workflow**](Workflow.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## indexWorkflows

> IndexWorkflows indexWorkflows(opts)

Index workflows

Index workflows

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowApi();
let opts = {
  'sort': "sort_example", // String | Use to sort results. You can specify by which value it will be sorted. example - \"sort=title:asc\" \"sort=createdAt:desc\"
  'page': 56, // Number | The number of the page you want to get. Default value = 1
  'pageSize': 56 // Number | The number of element a page can contains. Default value = 10
};
apiInstance.indexWorkflows(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort** | **String**| Use to sort results. You can specify by which value it will be sorted. example - \&quot;sort&#x3D;title:asc\&quot; \&quot;sort&#x3D;createdAt:desc\&quot; | [optional] 
 **page** | **Number**| The number of the page you want to get. Default value &#x3D; 1 | [optional] 
 **pageSize** | **Number**| The number of element a page can contains. Default value &#x3D; 10 | [optional] 

### Return type

[**IndexWorkflows**](IndexWorkflows.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## updateWorkflow

> updateWorkflow(workflowId, workflowUpdateInputs)

Update a workflow

Update a workflow.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowApi();
let workflowId = "workflowId_example"; // String | the uuid of the workflow
let workflowUpdateInputs = new WavestreamOpenapiSdkJs.WorkflowUpdateInputs(); // WorkflowUpdateInputs | workflow to update
apiInstance.updateWorkflow(workflowId, workflowUpdateInputs).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **String**| the uuid of the workflow | 
 **workflowUpdateInputs** | [**WorkflowUpdateInputs**](WorkflowUpdateInputs.md)| workflow to update | 

### Return type

null (empty response body)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/problem+json

