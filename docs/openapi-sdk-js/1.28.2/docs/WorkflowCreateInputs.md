# WavestreamOpenapiSdkJs.WorkflowCreateInputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the workflow | 
**description** | **String** | The description of the workflow | [optional] 


