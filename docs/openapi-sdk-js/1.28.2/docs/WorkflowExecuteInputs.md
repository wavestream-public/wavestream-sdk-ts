# WavestreamOpenapiSdkJs.WorkflowExecuteInputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aiaEncode** | [**AiaEncode**](AiaEncode.md) |  | [optional] 
**clips** | [**[ClipParams]**](ClipParams.md) |  | [optional] 
**overlays** | [**[OverlayParamsDeprecated]**](OverlayParamsDeprecated.md) |  | [optional] 
**publish** | [**PublishParams**](PublishParams.md) |  | [optional] 
**crops** | [**[CropParams]**](CropParams.md) |  | [optional] 


