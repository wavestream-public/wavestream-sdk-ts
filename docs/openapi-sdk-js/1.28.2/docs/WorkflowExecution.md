# WavestreamOpenapiSdkJs.WorkflowExecution

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id of the workflow execution | [optional] [readonly] 
**status** | **String** | The status of the workflow execution | [optional] [readonly] [default to &#39;running&#39;]
**error** | **String** | The error raised during the workflow execution | [optional] [readonly] 
**appId** | **String** | Id of the app which owns this workflow execution | [optional] [readonly] 
**startedAt** | **Date** | start time | [optional] [readonly] 
**endedAt** | **Date** | end time | [optional] [readonly] 
**tasks** | [**[WorkflowExecutionTasksInner]**](WorkflowExecutionTasksInner.md) |  | [readonly] 



## Enum: StatusEnum


* `scheduled` (value: `"scheduled"`)

* `running` (value: `"running"`)

* `completed` (value: `"completed"`)

* `failed` (value: `"failed"`)

* `canceled` (value: `"canceled"`)




