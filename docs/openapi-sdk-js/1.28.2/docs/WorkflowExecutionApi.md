# WavestreamOpenapiSdkJs.WorkflowExecutionApi

All URIs are relative to *https://api.wave.stream*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createWorkflowExecution**](WorkflowExecutionApi.md#createWorkflowExecution) | **POST** /workflow-executions | Create a workflow execution
[**getOneWorkflowExecution**](WorkflowExecutionApi.md#getOneWorkflowExecution) | **GET** /workflow-executions/{workflowExecutionId} | Get a workflow execution
[**indexWorkflowExecutions**](WorkflowExecutionApi.md#indexWorkflowExecutions) | **GET** /workflow-executions | Index workflow executions



## createWorkflowExecution

> WorkflowExecution createWorkflowExecution(opts)

Create a workflow execution

Create and execute a workflow.

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowExecutionApi();
let opts = {
  'workflowExecutionCreateInputs': new WavestreamOpenapiSdkJs.WorkflowExecutionCreateInputs() // WorkflowExecutionCreateInputs | Params to be passed to the run which will be executed.
};
apiInstance.createWorkflowExecution(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowExecutionCreateInputs** | [**WorkflowExecutionCreateInputs**](WorkflowExecutionCreateInputs.md)| Params to be passed to the run which will be executed. | [optional] 

### Return type

[**WorkflowExecution**](WorkflowExecution.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json


## getOneWorkflowExecution

> WorkflowExecution getOneWorkflowExecution(workflowExecutionId)

Get a workflow execution

Fetch a single workflow execution

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowExecutionApi();
let workflowExecutionId = "workflowExecutionId_example"; // String | the uuid of the workflow execution Id
apiInstance.getOneWorkflowExecution(workflowExecutionId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowExecutionId** | **String**| the uuid of the workflow execution Id | 

### Return type

[**WorkflowExecution**](WorkflowExecution.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json


## indexWorkflowExecutions

> IndexWorkflowExecutions indexWorkflowExecutions(opts)

Index workflow executions

Index workflow executions

### Example

```javascript
import WavestreamOpenapiSdkJs from '@wavestream/openapi-sdk-js';
let defaultClient = WavestreamOpenapiSdkJs.ApiClient.instance;
// Configure API key authorization: apiKeyAuth
let apiKeyAuth = defaultClient.authentications['apiKeyAuth'];
apiKeyAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKeyAuth.apiKeyPrefix = 'Token';

let apiInstance = new WavestreamOpenapiSdkJs.WorkflowExecutionApi();
let opts = {
  'sort': "sort_example", // String | Use to sort results. You can specify by which value it will be sorted. example - \"sort=title:asc\" \"sort=createdAt:desc\"
  'page': 56, // Number | The number of the page you want to get. Default value = 1
  'pageSize': 56 // Number | The number of element a page can contains. Default value = 10
};
apiInstance.indexWorkflowExecutions(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort** | **String**| Use to sort results. You can specify by which value it will be sorted. example - \&quot;sort&#x3D;title:asc\&quot; \&quot;sort&#x3D;createdAt:desc\&quot; | [optional] 
 **page** | **Number**| The number of the page you want to get. Default value &#x3D; 1 | [optional] 
 **pageSize** | **Number**| The number of element a page can contains. Default value &#x3D; 10 | [optional] 

### Return type

[**IndexWorkflowExecutions**](IndexWorkflowExecutions.md)

### Authorization

[apiKeyAuth](../README.md#apiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

