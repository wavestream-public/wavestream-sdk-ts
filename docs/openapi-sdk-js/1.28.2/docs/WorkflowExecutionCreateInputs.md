# WavestreamOpenapiSdkJs.WorkflowExecutionCreateInputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tasks** | [**[WorkflowExecutionCreateInputsTasksInner]**](WorkflowExecutionCreateInputsTasksInner.md) | Tasks to run | [optional] 


