# WavestreamOpenapiSdkJs.WorkflowUpdateInputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the workflow | [optional] 
**description** | **String** | The description of the workflow | [optional] 


