const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import VideoAPI from "./api/VideoApi";
import WebhookAPI from "./api/WebhookApi";
import RunAPI from "./api/RunApi";
import WorkflowAPI from "./api/WorkflowApi";
import AppAPI from "./api/AppApi";
import UserAPI from "./api/UserApi";
import WorkflowExecutionAPI from "./api/WorkflowExecutionApi";

export interface WavestreamAPIFuncsParams {
  constructorParams: { apiKey: string; endpoint?: string; accessToken: string };
}

export class WavestreamClient {
  video: InstanceType<typeof VideoAPI>;
  webhook: InstanceType<typeof WebhookAPI>;
  run: InstanceType<typeof RunAPI>;
  workflow: InstanceType<typeof WorkflowAPI>;
  app: InstanceType<typeof AppAPI>;
  user: InstanceType<typeof UserAPI>;
  workflowExecution: InstanceType<typeof WorkflowExecutionAPI>;

  openApiClient: InstanceType<typeof WavestreamOpenapiSdkJs.ApiClient>;
  private apiKey: string;
  endpoint: string;
  private accessToken: string;

  constructor({
    apiKey,
    endpoint = "https://api.wave.stream",
    accessToken,
  }: WavestreamAPIFuncsParams["constructorParams"]) {
    this.apiKey = apiKey;
    this.endpoint = endpoint;
    this.accessToken = accessToken;
    this.openApiClient = WavestreamOpenapiSdkJs.ApiClient.instance;

    this.setAuthentication();
    this.setBasePath();

    this.video = new VideoAPI({ apiKey: this.apiKey, endpoint: this.endpoint });
    this.webhook = new WebhookAPI();
    this.run = new RunAPI();
    this.workflow = new WorkflowAPI();
    this.app = new AppAPI();
    this.user = new UserAPI();
    this.workflowExecution = new WorkflowExecutionAPI({
      apiKey: this.apiKey,
      endpoint: this.endpoint,
    });
  }

  setAuthentication() {
    const apiKeyAuth = this.openApiClient.authentications["apiKeyAuth"];
    apiKeyAuth.apiKey = this.apiKey;

    const accessTokenAuth =
      this.openApiClient.authentications["accessTokenAuth"];
    accessTokenAuth.apiKey = this.accessToken;
  }

  setBasePath() {
    this.openApiClient.basePath = this.endpoint;
  }
}
