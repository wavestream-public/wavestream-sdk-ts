const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import throwSdkError from "../error/createSdkError";
import App from "../types/app";

interface AppAPIFuncsParams {
  appId: string;
  updateApp: {
    s3AccessKey?: string;
    S3SecretKey?: string;
    S3Region?: string;
    S3Endpoint?: string;
  };
}

export default class AppAPI {
  oaAppApi: InstanceType<typeof WavestreamOpenapiSdkJs.AppApi>;

  constructor() {
    this.oaAppApi = new WavestreamOpenapiSdkJs.AppApi();
  }

  index(): Promise<App[]> {
    return this.oaAppApi.indexApps().then(
      (data: App[]) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  getOne(appId: AppAPIFuncsParams["appId"]): Promise<App> {
    return this.oaAppApi.getOneApp(appId).then(
      (data: App) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  update(
    appId: AppAPIFuncsParams["appId"],
    updateParams: AppAPIFuncsParams["updateApp"]
  ): Promise<App> {
    return this.oaAppApi.updateApp(appId, updateParams).then(
      (data: App) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  renewApiKey(appId: AppAPIFuncsParams["appId"]): Promise<App> {
    return this.oaAppApi.renewAppSecret(appId).then(
      (data: App) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }
}
