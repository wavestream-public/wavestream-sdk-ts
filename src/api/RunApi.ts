const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import throwSdkError from "../error/createSdkError";
import Pagination, { constPagParams } from "../types/pagination";
import Run from "../types/run";

interface RunAPIFuncsParams {
  runId: string;
  runIndex: Pagination;
}

export default class RunAPI {
  oaRunApi: InstanceType<typeof WavestreamOpenapiSdkJs.RunApi>;

  constructor() {
    this.oaRunApi = new WavestreamOpenapiSdkJs.RunApi();
  }

  index(paginationParams?: RunAPIFuncsParams["runIndex"]): Promise<{
    items: Run[];
    pagination: Pagination;
  }> {
    return this.oaRunApi.indexRuns(constPagParams(paginationParams)).then(
      (data: { items: Run[]; pagination: Pagination }) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  getOne(runId: RunAPIFuncsParams["runId"]): Promise<Run> {
    return this.oaRunApi.getOneRun(runId).then(
      (data: Run) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }
}
