const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import throwSdkError from "../error/createSdkError";
import User from "../types/user";

export default class UserAPI {
  oaUserApi: InstanceType<typeof WavestreamOpenapiSdkJs.UserApi>;

  constructor() {
    this.oaUserApi = new WavestreamOpenapiSdkJs.UserApi();
  }

  me(): Promise<User> {
    return this.oaUserApi.usersMe().then(
      (data: User) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }
}
