const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import * as tus from "tus-js-client";
import Video from "../types/video";
import throwSdkError from "../error/createSdkError";
import Pagination, { constPagParams } from "../types/pagination";

export interface VideoAPIFuncsParams {
  constructor: { apiKey: string; endpoint: string };
  videoId: string;
  createVideo: {
    title: string;
    description?: string;
    skipProcessing?: boolean;
    accessType?: string;
  };
  videoIndex: Pagination;
  updateVideo: {
    title?: string;
    description?: string;
    skipProcessing?: boolean;
    accessType?: string;
  };
  uploadVideo: {
    videoId: string;
    file: File;
    uploadOptions: tus.UploadOptions;
  };
}

export default class VideoAPI {
  oaVideoApi: InstanceType<typeof WavestreamOpenapiSdkJs.VideoApi>;
  private apiKey: string;
  endpoint: string;

  constructor({ apiKey, endpoint }: VideoAPIFuncsParams["constructor"]) {
    this.apiKey = apiKey;
    this.endpoint = endpoint;
    this.oaVideoApi = new WavestreamOpenapiSdkJs.VideoApi();
  }

  create(
    createVideoParams: VideoAPIFuncsParams["createVideo"]
  ): Promise<Video> {
    return this.oaVideoApi.createVideo(createVideoParams).then(
      (data: Video) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  index(paginationParams?: VideoAPIFuncsParams["videoIndex"]): Promise<{
    items: Video[];
    pagination: Pagination;
  }> {
    return this.oaVideoApi.indexVideos(constPagParams(paginationParams)).then(
      (data: { items: Video[]; pagination: Pagination }) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  getOne(videoId: VideoAPIFuncsParams["videoId"]): Promise<Video> {
    return this.oaVideoApi.getOneVideo(videoId).then(
      (data: Video) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  update(
    videoId: VideoAPIFuncsParams["videoId"],
    updateParams: VideoAPIFuncsParams["updateVideo"]
  ): Promise<void> {
    const updateVideoOpts = {
      updateVideoRequest:
        new WavestreamOpenapiSdkJs.UpdateVideoRequest.constructFromObject(
          updateParams
        ),
    };
    return this.oaVideoApi.updateVideo(videoId, updateVideoOpts).then(
      () => {},
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  delete(videoId: VideoAPIFuncsParams["videoId"]): Promise<void> {
    return this.oaVideoApi.deleteVideo(videoId).then(
      () => {},
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  upload({
    videoId,
    file,
    uploadOptions,
  }: VideoAPIFuncsParams["uploadVideo"]): void {
    const { onProgress, onSuccess } = uploadOptions;

    function calculateParallelUpload(file: File) {
      const MAX_PARALLEL_UPLOAD = 10;
      const SIZE_THRESHOLD = 1 * 1024 * 1024 * 1024; // 1 Go

      const nbParallelUploads = Math.min(
        Math.ceil(file.size / SIZE_THRESHOLD),
        MAX_PARALLEL_UPLOAD
      );
      console.log("Number of parallel uploads", nbParallelUploads);

      return nbParallelUploads;
    }

    uploadOptions = {
      onProgress,
      onSuccess,
      endpoint: `${this.endpoint}/videos/${videoId}/source`,
      onError(error: tus.DetailedError | Error) {
        process.exitCode = 1;
        const errorDetailsObj = /{([^}]+)}/.exec(error.message);
        if (errorDetailsObj) {
          return throwSdkError({ body: JSON.parse(errorDetailsObj[0]) });
        } else {
          return throwSdkError(error);
        }
      },
      retryDelays: [0, 3000, 5000, 10000, 20000],
      headers: { "x-api-key": this.apiKey },
      chunkSize: 60 * 1024 * 1024, // 60 Mo
      parallelUploads: calculateParallelUpload(file),
    };

    const upload = new tus.Upload(file, uploadOptions);

    upload.findPreviousUploads().then(function (previousUploads) {
      if (previousUploads.length) {
        upload.resumeFromPreviousUpload(previousUploads[0]);
      }

      upload.start();
    });
  }
}
