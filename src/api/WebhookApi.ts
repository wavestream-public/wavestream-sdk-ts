const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import throwSdkError from "../error/createSdkError";
import Pagination, { constPagParams } from "../types/pagination";
import Webhook from "../types/webhook";

interface WebhookAPIFuncsParams {
  webhookId: string;
  webhookIndex: Pagination;
  createWebhook: { endpoint: string; events?: string[] };
  updateWebhook: { endpoint?: string; events?: string[] };
}

export default class WebhookAPI {
  oaWebhookApi: InstanceType<typeof WavestreamOpenapiSdkJs.WebhookApi>;

  constructor() {
    this.oaWebhookApi = new WavestreamOpenapiSdkJs.WebhookApi();
  }

  create(
    createWebhookParams: WebhookAPIFuncsParams["createWebhook"]
  ): Promise<Webhook> {
    return this.oaWebhookApi.createWebhook(createWebhookParams).then(
      (data: Webhook) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  index(paginationParams?: WebhookAPIFuncsParams["webhookIndex"]): Promise<{
    items: Webhook[];
    pagination: Pagination;
  }> {
    return this.oaWebhookApi
      .indexWebhooks(constPagParams(paginationParams))
      .then(
        (data: { items: Webhook[]; pagination: Pagination }) => {
          return data;
        },
        (error: any) => {
          return throwSdkError(error);
        }
      );
  }

  getOne(webhookId: WebhookAPIFuncsParams["webhookId"]): Promise<Webhook> {
    return this.oaWebhookApi.getOneWebhook(webhookId).then(
      (data: Webhook) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  update(
    webhookId: WebhookAPIFuncsParams["webhookId"],
    updateParams: WebhookAPIFuncsParams["updateWebhook"]
  ): Promise<void> {
    return this.oaWebhookApi.updateWebhook(webhookId, updateParams).then(
      () => {},
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  delete(webhookId: WebhookAPIFuncsParams["webhookId"]): Promise<void> {
    return this.oaWebhookApi.deleteWebhook(webhookId).then(
      () => {},
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }
}
