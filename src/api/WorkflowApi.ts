const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");
import throwSdkError from "../error/createSdkError";
import Workflow from "../types/workflow";
import Pagination, { constPagParams } from "../types/pagination";
import Run from "../types/run";

interface WorkflowAPIFuncsParams {
  workflowId: string;
  createWorkflow: { name: string; description?: string };
  workflowIndex: Pagination;
  updateWorkflow: { name?: string; description?: string };
  executeWorkflow: any;
}

export default class WorkflowAPI {
  oaWorkflowApi: InstanceType<typeof WavestreamOpenapiSdkJs.WorkflowApi>;

  constructor() {
    this.oaWorkflowApi = new WavestreamOpenapiSdkJs.WorkflowApi();
  }

  create(
    createWorkflowParams: WorkflowAPIFuncsParams["createWorkflow"]
  ): Promise<Workflow> {
    return this.oaWorkflowApi.createWorkflow(createWorkflowParams).then(
      (data: Workflow) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  index(paginationParams?: WorkflowAPIFuncsParams["workflowIndex"]): Promise<{
    items: Workflow[];
    pagination: Pagination;
  }> {
    return this.oaWorkflowApi
      .indexWorkflows(constPagParams(paginationParams))
      .then(
        (data: { items: Workflow[]; pagination: Pagination }) => {
          return data;
        },
        (error: any) => {
          return throwSdkError(error);
        }
      );
  }

  getOne(workflowId: WorkflowAPIFuncsParams["workflowId"]): Promise<Workflow> {
    return this.oaWorkflowApi.getOneWorkflow(workflowId).then(
      (data: Workflow) => {
        return data;
      },
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  update(
    workflowId: WorkflowAPIFuncsParams["workflowId"],
    updateParams: WorkflowAPIFuncsParams["updateWorkflow"]
  ): Promise<void> {
    return this.oaWorkflowApi.updateWorkflow(workflowId, updateParams).then(
      () => {},
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  delete(workflowId: WorkflowAPIFuncsParams["workflowId"]): Promise<void> {
    return this.oaWorkflowApi.deleteWorkflow(workflowId).then(
      () => {},
      (error: any) => {
        return throwSdkError(error);
      }
    );
  }

  execute(
    workflowId: WorkflowAPIFuncsParams["workflowId"],
    executeParams?: WorkflowAPIFuncsParams["executeWorkflow"]
  ): Promise<Run> {
    const executeParamsObj = {
      body: executeParams,
    };

    return this.oaWorkflowApi
      .executeWorkflow(workflowId, executeParamsObj)
      .then(
        (data: Run) => {
          return data;
        },
        (error: any) => {
          return throwSdkError(error);
        }
      );
  }
}
