import axios from "axios";
const WavestreamOpenapiSdkJs = require("@wavestream/openapi-sdk-js");

import throwSdkError from "../error/createSdkError";
import WorkflowExecution from "../types/workflowExecution";
import Pagination, { constPagParams } from "../types/pagination";

interface WorkflowExecutionAPIFuncsParams {
  constructor: { apiKey: string; endpoint: string };
  workflowExecutionId: string;
  workflowExecutionIndex: Pagination;
  createParams: Record<string, any> | string;
}

export default class WorkflowExecutionAPI {
  oaWorkflowExecutionApi: InstanceType<
    typeof WavestreamOpenapiSdkJs.WorkflowExecutionApi
  >;
  endpoint: string;
  private apiKey: string;

  constructor({
    apiKey,
    endpoint,
  }: WorkflowExecutionAPIFuncsParams["constructor"]) {
    this.apiKey = apiKey;
    this.endpoint = endpoint;
    this.oaWorkflowExecutionApi =
      new WavestreamOpenapiSdkJs.WorkflowExecutionApi();
  }

  create(
    createParams: WorkflowExecutionAPIFuncsParams["createParams"]
  ): Promise<WorkflowExecution> {
    // We call the API directly because the openAPI SDK is broken (oneOf issue)
    const url = `${this.endpoint}/workflow-executions`;
    return axios({
      method: "post",
      url,
      headers: {
        "x-api-key": this.apiKey,
        "content-type": "application/json",
      },
      data: createParams,
    }).then(res => res.data);
  }

  index(
    paginationParams?: WorkflowExecutionAPIFuncsParams["workflowExecutionIndex"]
  ): Promise<{
    items: WorkflowExecution[];
    pagination: Pagination;
  }> {
    return this.oaWorkflowExecutionApi
      .indexWorkflowExecutions(constPagParams(paginationParams))
      .then(
        (data: { items: WorkflowExecution[]; pagination: Pagination }) => {
          return data;
        },
        (error: any) => {
          return throwSdkError(error);
        }
      );
  }

  getOne(
    workflowExecutionId: WorkflowExecutionAPIFuncsParams["workflowExecutionId"]
  ): Promise<WorkflowExecution> {
    // We call the API directly because the openAPI SDK is broken (oneOf issue)
    const url = `${this.endpoint}/workflow-executions/${workflowExecutionId}`;
    return axios(url, {
      method: "GET",
      headers: {
        "x-api-key": this.apiKey,
      },
    }).then(res => res.data);
  }
}
