export interface SDKConstructorParams {
  message: string;
  type?: string;
  status?: number;
  detail?: string;
  title?: string;
}
export default class SdkError extends Error {
  name: string;
  type?: string;
  status?: number;
  detail?: string;
  title?: string;

  constructor({ message, status, type, detail, title }: SDKConstructorParams) {
    super(`Message: ${message}`);
    this.name = "Sdk Error";
    this.type = type;
    this.status = status;
    this.detail = detail;
    this.title = title;
  }
}
