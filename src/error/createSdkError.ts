import SdkError, { SDKConstructorParams } from "./SdkError";

function throwSdkError(inputError: any): never {
  if (!inputError.response) {
    throw new SdkError({ message: inputError.stack });
  }
  const { status, title, type, detail } = inputError.body;

  const errorConstructorParams: SDKConstructorParams = {
    message: inputError.response.text,
    status: status,
    title: title,
    type: type,
    detail: detail,
  };

  throw new SdkError(errorConstructorParams);
}

export default throwSdkError;
