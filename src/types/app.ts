type App = {
  id: string;
  name: string;
  secretKey: string;
  userId: string;
  createdAt: string;
  updatedAt: string;
  s3AccessKey: string;
  s3SecretKey: string;
  s3Region: string;
  s3Endpoint: string;
};

export default App;
