type Pagination = {
  sort?: string;
  page?: number;
  pageSize?: number;
  totalNbOfItems?: number;
};

export function constPagParams(
  paginationParams: Pagination | undefined
): Pagination {
  return {
    sort: paginationParams?.sort ? paginationParams.sort : undefined,
    page: paginationParams?.page ? paginationParams.page : 1,
    pageSize: paginationParams?.pageSize ? paginationParams.pageSize : 10,
  };
}
export default Pagination;
