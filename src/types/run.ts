import Workflow from "./workflow";

type Run = {
  id: string;
  status: string;
  params: Record<string, string>;
  workflowId: string;
  workflow: Workflow;
  appId: string;
  startedAt: string;
  endedAt: string;
  result: Record<string, string>;
};

export default Run;
