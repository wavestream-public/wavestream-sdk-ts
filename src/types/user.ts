type User = {
  id: string;
  auth0Id: string;
  username: string;
  email: string;
  avatarUri: string;
  createdAt: string;
  updatedAt: string;
};

export default User;
