export type Input = {
  id: string;
  label: string;
  format: string;
  videoCodec: string;
  audioCodec: string;
  quality: string;
  uri: string;
  duration: number;
  width: number;
  height: number;
  bitrate: string;
  aspectRatio: string;
  size: number;
};

export type Output = {
  id: string;
  label: string;
  format: string;
  videoCodec: string;
  audioCodec: string;
  quality: string;
  masterPlaylistUri: string;
  uri: string;
  duration: number;
  width: number;
  height: number;
  bitrate: number;
  aspectRatio: string;
  posterUri: string;
  size: number;
};

type Video = {
  id: string;
  title: string;
  description: string;
  status: string;
  skipProcessing: boolean;
  accessType: string;
  transcodedAt?: string;
  errorMessage?: string;
  destinationUri: string;
  input: Input | {};
  outputs: Output[] | [];
  appId: string;
  createdAt: string;
  updatedAt: string;
};

export default Video;
