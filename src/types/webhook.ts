type Webhook = {
  id: string;
  events: string[];
  endpoint: string;
  appId: string;
  createdAt: string;
  updatedAt: string;
};

export default Webhook;
