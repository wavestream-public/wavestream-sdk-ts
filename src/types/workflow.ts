type Workflow = {
  id: string;
  name: string;
  description: string;
  appId: string;
  createdAt: string;
  updatedAt: string;
};

export default Workflow;
