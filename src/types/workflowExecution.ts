export type Task = {
  createdAt: string;
  endedAt: string;
  id: string;
  label: string;
  params: Record<string, unknown>;
  results: Record<string, unknown>;
  startedAt: string;
  needs: string[];
  status: string;
  type: string;
  updatedAt: string;
};

type WorkflowExecution = {
  appId: string;
  id: string;
  startedAt: string;
  status: string;
  params: Record<string, unknown>;
  tasks: Task[];
  endedAt: string;
};

export default WorkflowExecution;
